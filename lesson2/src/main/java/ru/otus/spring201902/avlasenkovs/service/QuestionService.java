package ru.otus.spring201902.avlasenkovs.service;

import ru.otus.spring201902.avlasenkovs.model.Question;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface QuestionService {
    List<Question> getQuestions(File questionsFile) throws FileNotFoundException;

    String questionToString(String strQuestion, List<String> answers);

    boolean isCorrect(int answerNum, List<String> mixedAnswers, Question question);
}
