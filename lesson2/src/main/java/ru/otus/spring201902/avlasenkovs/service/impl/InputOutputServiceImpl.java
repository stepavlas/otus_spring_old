package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.service.InputOutputService;

import java.util.Scanner;

@Service
public class InputOutputServiceImpl implements InputOutputService {
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String fromInput() {
        return scanner.next();
    }

    @Override
    public String fromInput(String str) {
        System.out.println(str);
        return fromInput();
    }

    @Override
    public void toOutput(String str) {
        System.out.println(str);
    }
}
