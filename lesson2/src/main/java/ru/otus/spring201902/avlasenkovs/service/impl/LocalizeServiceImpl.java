package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

@Service
@PropertySource("classpath:test.properties")
public class LocalizeServiceImpl implements LocalizeService {
    private final MessageSource messageSource;
    private final Locale locale;
    private final File questionsFile;

    @Autowired
    public LocalizeServiceImpl(MessageSource messageSource, @Value("${language.locale}") String language,
                               @Value("${questions.file}") String questionsFileName) throws IOException {
        this.messageSource = messageSource;
        this.locale = new Locale(language.toLowerCase(), language.toUpperCase());
        ClassPathResource classPathResource = new ClassPathResource(questionsFileName);
        this.questionsFile = classPathResource.getFile();
        if (!questionsFile.exists()) {
            throw new FileNotFoundException(String.format("File with questions [%s] not found", questionsFile));
        }
    }

    @Override
    public String localize(String property, String... args) {
        return messageSource.getMessage(property, args, locale);
    }

    @Override
    public File getQuestionsFile() {
        return questionsFile;
    }

    @Override
    public String localizeStatistics(QuizStatistics quizStatistics) {
        final String numOfCorrectAnsw = String.valueOf(quizStatistics.numOfCorrectAnsw());
        final String numOfFailedAnsw = String.valueOf(quizStatistics.numOfFailedAnsw());
        final String numOfQuestions = String.valueOf(quizStatistics.numOfQuestions());
        return localize("statistics.msg", numOfCorrectAnsw, numOfFailedAnsw, numOfQuestions);
    }
}
