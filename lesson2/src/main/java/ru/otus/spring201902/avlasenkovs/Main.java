package ru.otus.spring201902.avlasenkovs;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.otus.spring201902.avlasenkovs.configuration.SpringConfiguration;
import ru.otus.spring201902.avlasenkovs.service.QuizService;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        QuizService quizService = applicationContext.getBean(QuizService.class);
        quizService.startQuiz();
    }
}
