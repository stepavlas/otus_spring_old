package ru.otus.spring201902.avlasenkovs.service;

import ru.otus.spring201902.avlasenkovs.model.User;

public interface UserService {
    User getUser();
}
