package ru.otus.spring201902.avlasenkovs.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Checking statistics count.")
public class QuizStatisticsTest {
    private QuizStatistics quizStatistics;

    @BeforeEach
    void setUp() {
        quizStatistics = new QuizStatistics();
    }


    @Test
    @DisplayName("check default")
    void checkCreate() {
        assertQuizAStatistics(quizStatistics, 0, 0, 0);
    }

    @Test
    @DisplayName("Increment number of answers")
    void checkNumIncrease() {
        for (int i = 0; i < 5; i++) {
            quizStatistics.correctAnswer();
        }
        for (int i = 0; i < 2; i++) {
            quizStatistics.failedAnswer();
        }
        assertQuizAStatistics(quizStatistics, 5, 2, 7);
    }

    private static void assertQuizAStatistics(QuizStatistics quizStatistics, int numOfCorrectAnsw,
                                             int numOfFailedAnsw, int numOfQuestions) {
        Assertions.assertAll(
                () ->  assertEquals(numOfCorrectAnsw, quizStatistics.numOfCorrectAnsw(),
                        "Incorrect number of correct answers"),
                () -> assertEquals(numOfFailedAnsw, quizStatistics.numOfFailedAnsw(),
                        "Incorrect number of failed answers"),
                () -> assertEquals( numOfQuestions, quizStatistics.numOfQuestions(),
                        "Incorrect number of all questions")
        );
    }
}
