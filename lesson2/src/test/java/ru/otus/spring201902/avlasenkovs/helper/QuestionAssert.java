package ru.otus.spring201902.avlasenkovs.helper;

import org.assertj.core.api.AbstractAssert;
import ru.otus.spring201902.avlasenkovs.model.Question;

import java.util.Iterator;
import java.util.List;

public class QuestionAssert extends AbstractAssert<QuestionAssert, Question> {
    private Iterator<String> answerIter;

    public QuestionAssert(Question question) {
        super(question, QuestionAssert.class);
    }

    public static QuestionAssert assertThat(Question actual) {
        return new QuestionAssert(actual);
    }

    public QuestionAssert hasStrQuestion(String strQuestion) {
        isNotNull();
        if (!actual.getStrQuestion().equals(strQuestion)) {
            failWithMessage("Expected string question is [%s] but was [%s]", strQuestion,
                    actual.getStrQuestion());
        }
        return this;
    }

    public QuestionAssert hasCorrectAnswer(short correctAnswer) {
        if (actual.getCorrectAnswer() != correctAnswer) {
            failWithMessage("Expected correct answer is [%d] but was [%d]", correctAnswer, actual.getCorrectAnswer());
        }
        return this;
    }

    public QuestionAssert hasAnswer(int answerNum, String answer) {
        isNotNull();
        List<String> answers = actual.getAnswers();
        if (answers == null) {
            failWithMessage("No answers found");
        }
        int answerPos = answerNum - 1;
        if (answers.size() <= answerPos) {
            failWithMessage("There is no [%d] answer.", answerNum);
        }
        if (!actual.getAnswers().get(answerPos).equals(answer)) {
            failWithMessage("Expected answer with num [%d] is [%s] but was [%s]", answerNum, answer,
                    actual.getAnswers().get(answerPos));
        }
        return this;
    }
}
