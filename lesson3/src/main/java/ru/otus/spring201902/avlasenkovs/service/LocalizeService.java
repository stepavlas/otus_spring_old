package ru.otus.spring201902.avlasenkovs.service;

import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;

import java.io.File;

public interface LocalizeService {
    String localize(String property, String... args);

    File getQuestionsFile();

    String localizeStatistics(QuizStatistics quizStatistics);
}
