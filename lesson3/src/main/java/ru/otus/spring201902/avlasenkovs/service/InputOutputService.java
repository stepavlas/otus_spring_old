package ru.otus.spring201902.avlasenkovs.service;

public interface InputOutputService {
    String fromInput();

    String fromInput(String str);

    void toOutput(String str);
}
