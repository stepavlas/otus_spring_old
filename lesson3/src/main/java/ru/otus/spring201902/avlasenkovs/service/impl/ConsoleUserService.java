package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.model.User;
import ru.otus.spring201902.avlasenkovs.service.InputOutputService;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;
import ru.otus.spring201902.avlasenkovs.service.UserService;

@Service
public class ConsoleUserService implements UserService {
    private InputOutputService inputOutputService;
    private LocalizeService localizeService;

    @Autowired
    public ConsoleUserService(InputOutputService inputOutputService, LocalizeService localizeService) {
        this.inputOutputService = inputOutputService;
        this.localizeService = localizeService;
    }

    @Override
    public User getUser() {
        final String firstName = inputOutputService.fromInput(localizeService.localize("first.name"));
        final String lastName = inputOutputService.fromInput(localizeService.localize("last.name"));
        return new User(firstName, lastName);
    }
}
