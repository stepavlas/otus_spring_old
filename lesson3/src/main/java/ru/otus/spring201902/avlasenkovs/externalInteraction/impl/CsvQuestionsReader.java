package ru.otus.spring201902.avlasenkovs.externalInteraction.impl;

import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Component;
import ru.otus.spring201902.avlasenkovs.externalInteraction.QuestionsReader;
import ru.otus.spring201902.avlasenkovs.model.Question;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@Component
public class CsvQuestionsReader implements QuestionsReader {
    @Override
    public List<Question> readQuestions(File questionsFile) throws FileNotFoundException {
        return new CsvToBeanBuilder<Question>(new FileReader(questionsFile))
                .withType(Question.class)
                .withSeparator('?')
                .build()
                .parse();
    }
}
