package ru.otus.spring201902.avlasenkovs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.otus.spring201902.avlasenkovs.service.QuizService;

@SpringBootApplication
public class Lesson3Application {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(Lesson3Application.class, args);
		QuizService quizService = applicationContext.getBean(QuizService.class);
        quizService.startQuiz();
	}

}
