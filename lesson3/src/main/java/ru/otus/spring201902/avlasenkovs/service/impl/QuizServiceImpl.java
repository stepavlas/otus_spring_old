package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.model.User;
import ru.otus.spring201902.avlasenkovs.service.*;

import java.io.FileNotFoundException;
import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {
    private final QuestionService questionService;
    private final InputOutputService inputOutputService;
    private final LocalizeService localizeService;
    private final UserService userService;

    @Autowired
    public QuizServiceImpl(QuestionService questionService, InputOutputService inputOutputService, LocalizeService localizeService, UserService userService) {
        this.questionService = questionService;
        this.inputOutputService = inputOutputService;
        this.localizeService = localizeService;
        this.userService = userService;
    }

    @Override
    public void startQuiz() {
        User user = getUserInfo();
        final QuizStatistics quizStatistics = askQuestions();
        printTestResults(user, quizStatistics);
    }

    private User getUserInfo() {
        User user = userService.getUser();
        inputOutputService.toOutput(localizeService.localize("greeting.msg", user.getFirstName(),
                user.getLastName()));
        return user;
    }

    private QuizStatistics askQuestions() {
        final QuizStatistics quizStatistics = new QuizStatistics();
        final List<Question> questions;
        try {
            questions = questionService.getQuestions(localizeService.getQuestionsFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        questions.forEach(question -> {
            askQuestion(question, quizStatistics);
        });
        return quizStatistics;
    }

    private void askQuestion(Question question, QuizStatistics quizStatistics) {
        final List<String> answers = question.getMixedAnswers();
        final String strQuestion = questionService.questionToString(question.getStrQuestion(), answers);
        inputOutputService.toOutput(strQuestion);
        final int answerNum = getAnswerNum(question.getAnswers().size());
        if (questionService.isCorrect(answerNum, answers, question)) {
            quizStatistics.correctAnswer();
            inputOutputService.toOutput(localizeService.localize("answer.correct"));
        } else {
            quizStatistics.failedAnswer();
            inputOutputService.toOutput(localizeService.localize("answer.wrong",
                    question.getCorrectStrAnswer()));
        }
    }

    private void printTestResults(User user, QuizStatistics quizStatistics) {
        if (quizStatistics == null) return;
        inputOutputService.toOutput(localizeService.localize("bye.msg", user.getFirstName(),
                user.getLastName()));
        inputOutputService.toOutput(localizeService.localizeStatistics(quizStatistics));
    }

    private int getAnswerNum(int numOfAnswers) {
        while (true) {
            try {
                final String strAnswerNum = inputOutputService.fromInput();
                final int answerNum = Integer.parseInt(strAnswerNum);
                if (answerNum < 1 || answerNum > numOfAnswers) {
                    inputOutputService.toOutput(localizeService.localize("answer.absent"));
                } else {
                    return answerNum;
                }
            } catch (NumberFormatException nfe) {
                inputOutputService.toOutput( localizeService.localize("answer.num"));
            }
        }
    }
}
