package ru.otus.spring201902.avlasenkovs.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Checking question methods.")
public class QuestionTest {
    private Question question;

    @BeforeEach
    void setUp() {
        question = new Question();
        question.setStrQuestion("Do you like writing unit tests?");
        List<String> answers = Arrays.asList("Definitely", "Not so sure", "Nope", "Definitely maybe");
        question.setAnswers(answers);
        question.setCorrectAnswer((short)4);
    }

    @Test
    @DisplayName("Check mixed answers contain same number of elements as in origin list")
    void checkMixedAnswersSize() {
        List<String> mixedAnswers = question.getMixedAnswers();
        List<String> originAnswers = question.getAnswers();
        assertEquals(mixedAnswers.size(), originAnswers.size());
    }

    @Test
    @DisplayName("Check mixed answers contain all answers from origin list")
    void checkMixedAnswersContain() {
        List<String> mixedAnswers = question.getMixedAnswers();
        List<String> originAnswers = question.getAnswers();
        assertTrue(mixedAnswers.containsAll(originAnswers));
    }

    @Test
    @DisplayName("Check mixed answers order changed")
    void checkMixedAnswersOrder() {
        List<String> mixedAnswers = question.getMixedAnswers();
        List<String> originAnswers = question.getAnswers();
        boolean changed = false;
        for (int i = 0; i < 100; i++) {
            if (!mixedAnswers.equals(originAnswers)) {
                changed = true;
                break;
            }
        }
        assertTrue(changed);
    }

    @Test
    @DisplayName(("Check get correct string answer"))
    void checkGetCorrectStrAnswer() {
        String correctStrAnswer = question.getCorrectStrAnswer();
        assertEquals("Definitely maybe", correctStrAnswer);
    }
}
