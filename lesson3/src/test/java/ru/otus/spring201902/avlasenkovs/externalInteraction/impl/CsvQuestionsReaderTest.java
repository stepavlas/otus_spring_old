package ru.otus.spring201902.avlasenkovs.externalInteraction.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import ru.otus.spring201902.avlasenkovs.externalInteraction.QuestionsReader;
import ru.otus.spring201902.avlasenkovs.model.Question;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static ru.otus.spring201902.avlasenkovs.helper.QuestionAssert.assertThat;

@DisplayName("Checking reading of csv questions files")
public class CsvQuestionsReaderTest {
    private QuestionsReader questionsReader;

    @BeforeEach
    void setUp() {
        questionsReader = new CsvQuestionsReader();
    }

    @Test
    @DisplayName("Check that records read from file.")
    void checkQuetionsRead() throws IOException {
        List<Question> questions = readQuestionsFile();
        assertEquals(5, questions.size());
    }

    private List<Question> readQuestionsFile() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("testQuestions.csv");
        List<Question> questions = questionsReader.readQuestions(classPathResource.getFile());
        assertNotNull(questions);
        return questions;
    }

    @Test
    @DisplayName("Check read question with 4 answers")
    void checkQuestionWith4Answers() throws IOException {
        List<Question> questions = readQuestionsFile();
        Question question = questions.get(0);
        assertThat(question)
                .hasStrQuestion("Some test question 1")
                .hasCorrectAnswer((short)3)
                .hasAnswer(1, "answer 1")
                .hasAnswer(2, "answer 2")
                .hasAnswer(3, "answer 3")
                .hasAnswer(4, "answer 4");
    }

    @Test
    @DisplayName("Check read question with 3 answers")
    void checkQuestionWith3Answers() throws IOException {
        List<Question> questions = readQuestionsFile();
        Question question = questions.get(3);
        assertThat(question)
                .hasStrQuestion("Some test question 4")
                .hasCorrectAnswer((short)4)
                .hasAnswer(1, "answer 1")
                .hasAnswer(2, "answer 2")
                .hasAnswer(3, "answer 3");

    }

    @Test
    @DisplayName("Check read question with 5 answers")
    void checkQuestionWith5Answers() throws IOException {
        List<Question> questions = readQuestionsFile();
        Question question = questions.get(1);
        assertThat(question)
                .hasStrQuestion("Some test question 2")
                .hasCorrectAnswer((short)2)
                .hasAnswer(1, "answer 1")
                .hasAnswer(2, "answer 2")
                .hasAnswer(3, "answer 3")
                .hasAnswer(4, "answer 4")
                .hasAnswer(5, "answer 5");
    }

    @Test
    @DisplayName("Check reading file when icorrect file name passed")
    void checkReadingWithIncorrectFileName() {
        assertThrows(FileNotFoundException.class,
                () -> questionsReader.readQuestions(new File("incorrect file name")));

    }

    @Test
    @DisplayName("Check reading incorrect file")
    void checkReadingWithIncorrectFile(){
        ClassPathResource classPathResource = new ClassPathResource("incorrectQuestions.csv");
        assertThrows(RuntimeException.class,
                () -> questionsReader.readQuestions(classPathResource.getFile()));
    }
}
