package ru.otus.spring201902.avlasenkovs.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.otus.spring201902.avlasenkovs.configuration.SpringConfiguration;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = {LocalizeServiceImpl.class, SpringConfiguration.class},
        properties = {"language.locale=ru", "questions.file=testQuestions_ru_RU.csv"})
public class LocalizeServiceImplTest {

    @Autowired
    private LocalizeService localizeService;

    @Test
    public void testLocalizedMethod1WithoutArguments() {
        String textMessage = localizeService.localize("text.message2");
        assertEquals("это второе сообщение", textMessage);
    }

    @Test
    public void testLocalizedMethodWithArguments() {
        String textMessage = localizeService.localize("text.message1", "первое", "на русском");
        assertEquals("это первое сообщение на русском", textMessage);
    }

    @Test
    public void getetQuestionsFileTest() {
        File questionsFile = localizeService.getQuestionsFile();
        Assertions.assertAll(
                () -> assertTrue(questionsFile.exists()),
                () -> assertEquals("testQuestions_ru_RU.csv", questionsFile.getName())
        );
    }

    @Test
    public void localizeStatisticsTest() {
        QuizStatistics quizStatistics = new QuizStatistics();
        for (int i = 0; i < 3; i++) {
            quizStatistics.correctAnswer();
        }
        for (int i = 0; i < 2; i++) {
            quizStatistics.failedAnswer();
        }
        String statisticsMsg = localizeService.localizeStatistics(quizStatistics);
        assertEquals("Ваш результат: количество правильных ответов [3], " +
                "количество неправильных ответов [2], общее число вопросов [5]", statisticsMsg);
    }

}
