package ru.otus.spring201902.avlasenkovs.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.spring201902.avlasenkovs.externalInteraction.QuestionsReader;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.service.QuestionService;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayName("Testing question service")
@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplTest {
    private QuestionService questionService;
    private Question question;

    @Mock
    private QuestionsReader questionsReader;

    @BeforeEach
    void setUp() {
        questionService = new QuestionServiceImpl(questionsReader);
        createQuestions();
    }

    private void createQuestions() {
        question = new Question();
        question.setStrQuestion("Is it good to stay up late?");
        List<String> answers = Arrays.asList("Definitely", "Not so sure", "Nope", "Definitely maybe");
        question.setAnswers(answers);
        question.setCorrectAnswer((short)3);
    }

    @Test
    @DisplayName("Correct answer test")
    void checkCorrectAnswerTest() {
        List<String> answers = question.getMixedAnswers();
        int answerNum = answers.indexOf("Nope") + 1;
        assertTrue(questionService.isCorrect(answerNum, answers, question));
    }

    @Test
    @DisplayName("Incorrect answer test")
    void checkIncorrectAnswerTest() {
        List<String> answers = question.getMixedAnswers();
        int answerNum = answers.indexOf("Not so sure") + 1;
        assertFalse(questionService.isCorrect(answerNum, answers, question));
    }
}
