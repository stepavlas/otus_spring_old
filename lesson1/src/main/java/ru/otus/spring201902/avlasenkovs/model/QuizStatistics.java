package ru.otus.spring201902.avlasenkovs.model;

public class QuizStatistics {
    private int correct;
    private int failed;

    public void correctAnswer() {
        correct++;
    }

    public void failedAnswer() {
        failed++;
    }

    @Override
    public String toString() {
        return String.format("number of correct answers [%d], number of failed answers [%d], " +
                "total number of questions [%d]", correct, failed, correct + failed);
    }
}
