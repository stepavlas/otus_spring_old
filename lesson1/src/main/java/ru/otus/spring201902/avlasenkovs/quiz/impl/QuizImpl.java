package ru.otus.spring201902.avlasenkovs.quiz.impl;

import org.springframework.core.io.ClassPathResource;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.questionsReader.QuestionsReader;
import ru.otus.spring201902.avlasenkovs.quiz.Quiz;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class QuizImpl implements Quiz {
    private QuestionsReader questionsReader;
    private File questionsFile;

    public QuizImpl(QuestionsReader questionsReader, ClassPathResource classPathResource) throws IOException {
        this.questionsReader = questionsReader;
        this.questionsFile = classPathResource.getFile();
    }

    @Override
    public void startQuiz() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your first name: ");
        String firstName = scanner.next();
        System.out.println("Enter your last name: ");
        String lastName = scanner.next();
        System.out.printf("Hi, %s %s! Let's start the quiz...%n", firstName, lastName);
        QuizStatistics quizStatistics = new QuizStatistics();
        try {
            List<Question> questions = questionsReader.readQuestions(questionsFile);
            questions.forEach((question -> {
                String answer = askQuestion(scanner, question);
                checkAnswer(answer, question, quizStatistics);
            }));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.printf("Quiz finished. Thanks for participation, %s %s! Your result: %s.%n", firstName, lastName,
                quizStatistics);
    }

    private void checkAnswer(String answer, Question question, QuizStatistics quizStatistics) {
        String correctAnswer = question.getCorrectStrAnswer();
        if (answer.equals(correctAnswer)) {
            System.out.printf("Answer is correct.%n%n");
            quizStatistics.correctAnswer();
        } else {
            System.out.printf("Incorrect answer. Right answer is [%s].%n%n", correctAnswer);
            quizStatistics.failedAnswer();
        }
    }

    private String askQuestion(Scanner scanner, Question question) {
        System.out.println(question.getStrQuestion() + '?');
        List<String> answers = question.getMixedAnswers();
        for (int i = 0; i < answers.size(); i++) {
            String answer = answers.get(i);
            int variantNum = i + 1;
            System.out.println(variantNum + ": " + answer);
        }
        int answerNum = getAnswerNum(scanner, answers.size());
        return answers.get(answerNum - 1);
    }

    private int getAnswerNum(Scanner scanner, int numOfAnswers) {
        while (true) {
            try {
                String strAnswerNum = scanner.next();
                int answerNum = Integer.parseInt(strAnswerNum);
                if (answerNum < 1 || answerNum > numOfAnswers) {
                    System.out.println("No such answer. Try again.");
                } else {
                    return answerNum;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Answer must be a number. Try again.");
            }
        }
    }
}
