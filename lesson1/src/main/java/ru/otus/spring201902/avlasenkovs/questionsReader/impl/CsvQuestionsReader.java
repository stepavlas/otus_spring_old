package ru.otus.spring201902.avlasenkovs.questionsReader.impl;

import com.opencsv.bean.CsvToBeanBuilder;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.questionsReader.QuestionsReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class CsvQuestionsReader implements QuestionsReader {
    @Override
    public List<Question> readQuestions(File questionsFile) throws FileNotFoundException {
        return new CsvToBeanBuilder<Question>(new FileReader(questionsFile))
                .withType(Question.class)
                .withSeparator('?')
                .build()
                .parse();
    }
}
