package ru.otus.spring201902.avlasenkovs;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.spring201902.avlasenkovs.quiz.Quiz;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");
        Quiz quiz = applicationContext.getBean(Quiz.class);
        quiz.startQuiz();
    }
}
