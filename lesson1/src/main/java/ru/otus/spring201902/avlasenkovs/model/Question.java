package ru.otus.spring201902.avlasenkovs.model;

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Question {
    @CsvBindByName(column = "Question")
    private String strQuestion;

    @CsvBindByName(column = "Correct Answer")
    private short correctAnswer;

    @CsvBindAndSplitByName (column = "Answer Variants", elementType = String.class, splitOn = ":+",
            collectionType = ArrayList.class)
    private List<String> answers;

    public String getStrQuestion() {
        return strQuestion;
    }

    public void setStrQuestion(String strQuestion) {
        this.strQuestion = strQuestion;
    }

    public void setCorrectAnswer(short correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public short getCorrectAnswer() {
        return correctAnswer;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public List<String> getMixedAnswers() {
        List<String> answersCopy = new ArrayList<>(answers);
        Collections.shuffle(answersCopy);
        return answersCopy;
    }

    public String getCorrectStrAnswer() {
        return answers.get(correctAnswer - 1);
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "strQuestion='" + strQuestion + '\'' +
                ", correctAnswer='" + correctAnswer + '\'' +
                ", answers=" + answers +
                '}';
    }
}
