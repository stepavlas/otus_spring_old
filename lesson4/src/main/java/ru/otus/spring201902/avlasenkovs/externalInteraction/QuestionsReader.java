package ru.otus.spring201902.avlasenkovs.externalInteraction;



import ru.otus.spring201902.avlasenkovs.model.Question;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface QuestionsReader {
    List<Question> readQuestions(File questionsFile) throws FileNotFoundException;
}
