package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.externalInteraction.QuestionsReader;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.service.QuestionService;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionsReader questionsReader;

    @Autowired
    public QuestionServiceImpl(QuestionsReader questionsReader) {
        this.questionsReader = questionsReader;
    }

    @Override
    public List<Question> getQuestions(File questionsFile) throws FileNotFoundException {
        return questionsReader.readQuestions(questionsFile);
    }

    @Override
    public String questionToString(String strQuestion, List<String> answers) {
        final StringBuilder completeQuestion = new StringBuilder(strQuestion + "?");
        for (int i = 0; i < answers.size(); i++) {
            completeQuestion.append(String.format("%n"));
            final String answer = answers.get(i);
            final int variantNum = i + 1;
            completeQuestion.append(variantNum);
            completeQuestion.append(": ");
            completeQuestion.append(answer);
        }
        return completeQuestion.toString();
    }

    @Override
    public boolean isCorrect(int answerNum, List<String> mixedAnswers, Question question) {
        final String chosenAnswer = mixedAnswers.get(answerNum - 1);
        return question.getCorrectStrAnswer().equals(chosenAnswer);
    }
}
