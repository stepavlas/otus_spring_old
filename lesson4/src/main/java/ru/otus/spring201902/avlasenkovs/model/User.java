package ru.otus.spring201902.avlasenkovs.model;

import java.util.Locale;

public class User {
    private final String firstName;
    private final String lastName;
    private Locale locale;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.locale = Locale.getDefault();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLanguage(String language) {
        this.locale = new Locale(language.toLowerCase(), language.toUpperCase());
    }

    public Locale getLocale() {
        return locale;
    }
}
