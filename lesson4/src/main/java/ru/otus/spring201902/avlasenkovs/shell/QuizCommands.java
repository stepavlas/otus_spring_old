package ru.otus.spring201902.avlasenkovs.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import ru.otus.spring201902.avlasenkovs.model.User;
import ru.otus.spring201902.avlasenkovs.service.QuizService;

@ShellComponent
public class QuizCommands {
    private final QuizService quizService;
    private User user;
    private String language;

    @Autowired
    public QuizCommands(QuizService quizService) {
        this.quizService = quizService;
    }

    @ShellMethod("login with your firstname/lastname")
    public void login(@ShellOption String firstName, @ShellOption String lastName) {
        user = new User(firstName, lastName);
    }

    @ShellMethod("get user")
    @ShellMethodAvailability("checkUser")
    public String getUser() {
        return user.getFirstName() + " " + user.getLastName();
    }

    @ShellMethod("choose language")
    public void setLanguage(@ShellOption String language) {
        this.language = language;
    }

    @ShellMethod("get language")
    @ShellMethodAvailability("checkLanguage")
    public String getLanguage() {
        return language;
    }

    @ShellMethod("start the quiz")
    @ShellMethodAvailability("checkUser")
    public void startQuiz() {
        if (language != null) {
            user.setLanguage(language);
        }
        quizService.startQuiz(user);
    }

    public Availability checkUser() {
        return user != null ? Availability.available() : Availability.unavailable("user not specified.");
    }

    public Availability checkLanguage() {
        return language != null ? Availability.available() : Availability.unavailable("language not specified");
    }
}
