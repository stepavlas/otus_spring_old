package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.model.Question;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.model.User;
import ru.otus.spring201902.avlasenkovs.service.InputOutputService;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;
import ru.otus.spring201902.avlasenkovs.service.QuestionService;
import ru.otus.spring201902.avlasenkovs.service.QuizService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

@Service
public class QuizServiceImpl implements QuizService {
    private final QuestionService questionService;
    private final InputOutputService inputOutputService;
    private final LocalizeService localizeService;

    @Autowired
    public QuizServiceImpl(QuestionService questionService, InputOutputService inputOutputService, LocalizeService localizeService) {
        this.questionService = questionService;
        this.inputOutputService = inputOutputService;
        this.localizeService = localizeService;
    }

    @Override
    public void startQuiz(User user) {
        inputOutputService.toOutput(localizeService.localize("greeting.msg", user.getLocale(),
                user.getFirstName(), user.getLastName()));
        final QuizStatistics quizStatistics = askQuestions(user.getLocale());
        printTestResults(user, quizStatistics);
    }

    private QuizStatistics askQuestions(Locale locale) {
        final QuizStatistics quizStatistics = new QuizStatistics();
        final List<Question> questions;
        try {
            questions = questionService.getQuestions(localizeService.getQuestionsFile(locale));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        questions.forEach(question -> askQuestion(question, quizStatistics, locale));
        return quizStatistics;
    }

    private void askQuestion(Question question, QuizStatistics quizStatistics, Locale locale) {
        final List<String> answers = question.getMixedAnswers();
        final String strQuestion = questionService.questionToString(question.getStrQuestion(), answers);
        inputOutputService.toOutput(strQuestion);
        final int answerNum = getAnswerNum(question.getAnswers().size(), locale);
        if (questionService.isCorrect(answerNum, answers, question)) {
            quizStatistics.correctAnswer();
            inputOutputService.toOutput(localizeService.localize("answer.correct", locale));
        } else {
            quizStatistics.failedAnswer();
            inputOutputService.toOutput(localizeService.localize("answer.wrong", locale,
                    question.getCorrectStrAnswer()));
        }
    }

    private void printTestResults(User user, QuizStatistics quizStatistics) {
        if (quizStatistics == null) return;
        inputOutputService.toOutput(localizeService.localize("bye.msg", user.getLocale(), user.getFirstName(),
                user.getLastName()));
        inputOutputService.toOutput(localizeService.localizeStatistics(quizStatistics, user.getLocale()));
    }

    private int getAnswerNum(int numOfAnswers, Locale locale) {
        while (true) {
            try {
                final String strAnswerNum = inputOutputService.fromInput();
                final int answerNum = Integer.parseInt(strAnswerNum);
                if (answerNum < 1 || answerNum > numOfAnswers) {
                    inputOutputService.toOutput(localizeService.localize("answer.absent", locale));
                } else {
                    return answerNum;
                }
            } catch (NumberFormatException nfe) {
                inputOutputService.toOutput( localizeService.localize("answer.num", locale));
            }
        }
    }
}
