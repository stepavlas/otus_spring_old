package ru.otus.spring201902.avlasenkovs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

@Service
public class LocalizeServiceImpl implements LocalizeService {
    private final MessageSource messageSource;
    private final String questionsFileName;

    @Autowired
    public LocalizeServiceImpl(MessageSource messageSource,
                               @Value("${questions.fileName}") String questionsFileName) throws IOException {
        Locale.setDefault(Locale.ENGLISH);
        this.messageSource = messageSource;
        this.questionsFileName = questionsFileName;
        String fullFileName = questionsFileName + ".csv";
        ClassPathResource classPathResource = new ClassPathResource(fullFileName);
        // check if file exists.
        try {
            classPathResource.getFile();
        } catch (FileNotFoundException fnfe) {
            throw new FileNotFoundException(String.format("Default questions file [%s] not found", fullFileName));
        }
    }

    @Override
    public String localize(String property, Locale locale, String... args) {
        return messageSource.getMessage(property, args, locale);
    }

    @Override
    public File getQuestionsFile(Locale locale) throws IOException {
        final ClassPathResource classPathResource;
        if (locale.equals(Locale.getDefault())) {
            classPathResource = new ClassPathResource(questionsFileName + ".csv");
        } else {
            classPathResource = new ClassPathResource(questionsFileName + "_" + locale + ".csv");
        }
        try {
            return classPathResource.getFile();
        } catch (FileNotFoundException fnfe) {
            final ClassPathResource defaultClassPathResource = new ClassPathResource(questionsFileName + ".csv");
            return defaultClassPathResource.getFile();
        }
    }

    @Override
    public String localizeStatistics(QuizStatistics quizStatistics, Locale locale) {
        final String numOfCorrectAnsw = String.valueOf(quizStatistics.numOfCorrectAnsw());
        final String numOfFailedAnsw = String.valueOf(quizStatistics.numOfFailedAnsw());
        final String numOfQuestions = String.valueOf(quizStatistics.numOfQuestions());
        return localize("statistics.msg", locale, numOfCorrectAnsw, numOfFailedAnsw, numOfQuestions);
    }
}
