package ru.otus.spring201902.avlasenkovs.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.otus.spring201902.avlasenkovs")
public class SpringAnnotationConfiguration {

}
