package ru.otus.spring201902.avlasenkovs.service;

import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public interface LocalizeService {
    String localize(String property, Locale locale, String... args);

    File getQuestionsFile(Locale locale) throws IOException;

    String localizeStatistics(QuizStatistics quizStatistics, Locale locale);
}
