package ru.otus.spring201902.avlasenkovs.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.otus.spring201902.avlasenkovs.configuration.SpringJavaConfiguration;
import ru.otus.spring201902.avlasenkovs.model.QuizStatistics;
import ru.otus.spring201902.avlasenkovs.service.LocalizeService;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = {LocalizeServiceImpl.class, SpringJavaConfiguration.class},
        properties = "questions.fileName=testQuestions")
public class LocalizeServiceImplTest {

    @Autowired
    private LocalizeService localizeService;

    @Test
    public void testLocalizedMethod1WithoutArguments() {
        Locale locale = new Locale("ru", "RU");
        String textMessage = localizeService.localize("text.message2", locale);
        assertEquals("это второе сообщение", textMessage);
    }

    @Test
    public void testLocalizedMethodWithArguments() {
        Locale locale = new Locale("ru", "RU");
        String textMessage = localizeService.localize("text.message1", locale, "первое", "на русском");
        assertEquals("это первое сообщение на русском", textMessage);
    }

    @Test
    public void testLocalizeMethodWithEnglishLocale() {
        Locale locale = new Locale("en", "EN");
        String textMessage = localizeService.localize("text.message1", locale);
        assertEquals("this is first message", textMessage);
    }

    @Test
    public void testLocalizeMethodWithUnknownLocale() {
        Locale locale = new Locale("dem", "DEN");
        String textMessage = localizeService.localize("text.message2", locale);
        assertEquals("this is second message", textMessage);
    }

    @Test
    public void testGetQuestionsFileWithRuLocale() throws IOException {
        Locale locale = new Locale("ru", "RU");
        checkQuestionFile(locale, "testQuestions_ru_RU.csv");
    }

    @Test
    public void testGetQuestionsFileWithEnglishLocale() throws IOException {
        Locale locale = new Locale("en", "EN");
        checkQuestionFile(locale, "testQuestions.csv");
    }

    @Test
    public void testGetQuestionsFileWithUnknownLocale() throws IOException {
        Locale locale = new Locale("de", "DE");
        checkQuestionFile(locale, "testQuestions.csv");
    }

    private void checkQuestionFile(Locale locale, String fileName) throws IOException {
        File questionsFile = localizeService.getQuestionsFile(locale);
        Assertions.assertAll(
                () -> assertTrue(questionsFile.exists()),
                () -> assertEquals(fileName, questionsFile.getName())
        );
    }

    @Test
    public void localizeStatisticsWithRuLocaleTest() {
        Locale locale = new Locale("ru", "RU");
        QuizStatistics quizStatistics = getStatistics(3, 2);
        String statisticsMsg = localizeService.localizeStatistics(quizStatistics, locale);
        assertEquals("Ваш результат: количество правильных ответов [3], " +
                "количество неправильных ответов [2], общее число вопросов [5]", statisticsMsg);
    }

    @Test
    public void localizeStatisticsWithEnLocaleTest() {
        Locale locale = new Locale("en", "en");
        QuizStatistics quizStatistics = getStatistics(5, 4);
        String statisticsMsg = localizeService.localizeStatistics(quizStatistics, locale);
        assertEquals("Your result: number of correct answers [5], " +
                "number of failed answers [4], total number of questions [9]", statisticsMsg);
    }

    @Test
    public void localizeStatisticsWithUnkownLocaleTest() {
        Locale locale = new Locale("den", "DEN");
        QuizStatistics quizStatistics = getStatistics(2, 2);
        String statisticsMsg = localizeService.localizeStatistics(quizStatistics, locale);
        assertEquals("Your result: number of correct answers [2], " +
                "number of failed answers [2], total number of questions [4]", statisticsMsg);
    }

    private QuizStatistics getStatistics(int correctAnswersNum, int failedAnswersNum) {
        QuizStatistics quizStatistics = new QuizStatistics();
        for (int i = 0; i < correctAnswersNum; i++) {
            quizStatistics.correctAnswer();
        }
        for (int i = 0; i < failedAnswersNum; i++) {
            quizStatistics.failedAnswer();
        }
        return quizStatistics;
    }

}
