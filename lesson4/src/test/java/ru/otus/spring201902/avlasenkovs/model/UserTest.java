package ru.otus.spring201902.avlasenkovs.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
    private User user;

    @BeforeEach
    void setUp() {
        user = new User("Sergei", "Pirate");
    }

    @Test
    @DisplayName("check default language")
    public void checkDefaultLanguage() {
        assertEquals(Locale.getDefault(), user.getLocale());
    }

    @Test
    @DisplayName("set language test")
    public void setLanguageTest() {
        user.setLanguage("en");
        assertEquals(new Locale("en", "EN"), user.getLocale());
    }
}
