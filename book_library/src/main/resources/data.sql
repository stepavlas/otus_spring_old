INSERT INTO genre
(genre)
VALUES
('Psychological Fiction'),
('Fantasy Fiction'),
('Satire'),
('Historical novel'),
('Realist novel'),
('Fictional Autobiography'),
('Fiction'),
('Poem'),
('Fairy tale'),
('Comedy'),
('Drama'),
('Play'),
('Vaudeville'),
('War novel'),
('Romance novel'),
('Detective fiction'),
('Science fiction'),
('Romanticism'),
('Adventure fiction');

INSERT INTO lang
(language)
VALUES
('chinese'),
('spanish'),
('english'),
('hindi'),
('arabic'),
('portuguese'),
('bengali'),
('russian'),
('japanese'),
('lahnda'),
('javanese'),
('german'),
('korean'),
('french'),
('turkish'),
('vietnamese');

INSERT INTO author
(first_name, last_name, birth_date, about_link)
VALUES
('Fyodor', 'Dostoevsky', '1821-11-11', 'https://en.wikipedia.org/wiki/Fyodor_Dostoevsky'),
('Mikhail', 'Bulgakov', '1891-05-15', 'https://en.wikipedia.org/wiki/Mikhail_Bulgakov'),
('Leo', 'Tolstoy', '1828-08-28', 'https://en.wikipedia.org/wiki/Leo_Tolstoy'),
('Alexander', 'Pushkin', '1799-06-06', 'https://en.wikipedia.org/wiki/Alexander_Pushkin'),
('Anton', 'Chekhov', '1860-01-29', 'https://en.wikipedia.org/wiki/Anton_Chekhov'),
('Erich Maria', 'Remarque', '1898-06-22', 'https://en.wikipedia.org/wiki/Erich_Maria_Remarque'),
('Alexandre', 'Dumas', '1802-07-24', 'https://en.wikipedia.org/wiki/Alexandre_Dumas'),
('Arthur Conan', 'Doyle', '1859-05-22', 'https://en.wikipedia.org/wiki/Arthur_Conan_Doyle'),
('Victor', 'Hugo', '1802-02-26', 'https://en.wikipedia.org/wiki/Victor_Hugo'),
('Jack', 'London', '1876-01-12', 'https://en.wikipedia.org/wiki/Jack_London'),
('Ernest', 'Hemingway', '1899-07-21', 'https://en.wikipedia.org/wiki/Ernest_Hemingway');

INSERT INTO book
(name, lang_fk, year, genre_fk, author_fk, about_link)
VALUES
('Crime and Punishment', 8, 1866, 1, 1, 'https://en.wikipedia.org/wiki/Crime_and_Punishment'),
('The Idiot', 8, 1869, 1, 1, 'https://en.wikipedia.org/wiki/The_Idiot'),
('The Master and Margarita', 8, 1967, 2, 2, 'https://en.wikipedia.org/wiki/The_Master_and_Margarita'),
('Heart of a Dog', 8, 1968, 3, 2, 'https://en.wikipedia.org/wiki/Heart_of_a_Dog'),
('War and Peace', 8, 1869, 4, 3, 'https://en.wikipedia.org/wiki/War_and_Peace'),
('Anna Karenina', 8, 1877, 5, 3, 'https://en.wikipedia.org/wiki/Anna_Karenina'),
('Childhood', 8, 1852, 6, 3, 'https://en.wikipedia.org/wiki/Childhood_(novel)'),
('The Captain''s Daughter', 8, 1836, 4, 4, 'https://en.wikipedia.org/wiki/The_Captain%27s_Daughter'),
('The Queen of Spades', 8, 1834, 7, 4, 'https://en.wikipedia.org/wiki/The_Queen_of_Spades_(story)'),
('Ruslan and Ludmila', 8, 1820, 8, 4, 'https://en.wikipedia.org/wiki/Ruslan_and_Ludmila'),
('The Tale of Tsar Saltan', 8, 1831, 9, 4, 'https://en.wikipedia.org/wiki/The_Tale_of_Tsar_Saltan'),
('The Seagull', 8, 1896, 10, 5, 'https://en.wikipedia.org/wiki/The_Seagull'),
('The Cherry Orchard', 8, 1904, 11, 5, 'https://en.wikipedia.org/wiki/The_Cherry_Orchard'),
('Three Sisters', 8, 1901, 11, 5, 'https://en.wikipedia.org/wiki/Three_Sisters_(play)'),
('Uncle Vanya', 8, 1898, 12, 5, 'https://en.wikipedia.org/wiki/Three_Sisters_(play)'),
('The Bear', 8, 1888, 13, 5, 'https://en.wikipedia.org/wiki/The_Bear_(play)'),
('All Quiet on the Western Front', 12, 1929, 14, 6, 'https://en.wikipedia.org/wiki/All_Quiet_on_the_Western_Front'),
('Three Comrades', 12, 1936, 14, 6, 'https://en.wikipedia.org/wiki/Three_Comrades_(novel)'),
('Arch of Triumph', 12, 1945, 14, 6, 'https://en.wikipedia.org/wiki/Arch_of_Triumph_(novel)'),
('Heaven Has No Favorites', 12, 1961, 15, 6, 'https://en.wikipedia.org/wiki/Heaven_Has_No_Favorites'),
('The Black Obelisk', 12, 1956, 7, 6, 'https://en.wikipedia.org/wiki/The_Black_Obelisk'),
('The Count of Monte Cristo', 14, 1844, 4, 7, 'https://en.wikipedia.org/wiki/The_Count_of_Monte_Cristo'),
('The Three Musketeers', 14, 1844, 4, 7, 'https://en.wikipedia.org/wiki/The_Three_Musketeers'),
('Twenty Years After', 14, 1845, 4, 7, 'https://en.wikipedia.org/wiki/Twenty_Years_After'),
('The Adventures of Sherlock Holmes', 3, 1892, 16, 8, 'https://en.wikipedia.org/wiki/The_Adventures_of_Sherlock_Holmes'),
('The Hound of the Baskervilles', 3, 1902, 16, 8, 'https://en.wikipedia.org/wiki/The_Hound_of_the_Baskervilles'),
('The Lost World', 3, 1912, 17, 8, 'https://en.wikipedia.org/wiki/The_Lost_World_(Conan_Doyle_novel)'),
('Les Misérables', 14, 1862, 4, 9, 'https://en.wikipedia.org/wiki/Les_Mis%C3%A9rables'),
('The Hunchback of Notre Dame', 14, 1831, 18, 9, 'https://en.wikipedia.org/wiki/The_Hunchback_of_Notre-Dame'),
('The call of the wild', 3, 1903, 19, 10, 'https://en.wikipedia.org/wiki/The_Call_of_the_Wild'),
('White Fang', 3, 1906, 19, 10, 'https://en.wikipedia.org/wiki/White_Fang'),
('Martin Eden', 3, 1909, 7, 10, 'https://en.wikipedia.org/wiki/Martin_Eden');






