package ru.otus.spring201902.avlasenkovs.repository;

import org.springframework.data.repository.CrudRepository;
import ru.otus.spring201902.avlasenkovs.model.Language;

import java.util.List;
import java.util.Optional;

public interface LangRepository extends CrudRepository<Language, Integer> {
    Optional<Language> findByName(String language);

    @Override
    List<Language> findAll();
}
