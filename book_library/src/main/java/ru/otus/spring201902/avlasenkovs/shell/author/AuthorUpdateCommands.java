package ru.otus.spring201902.avlasenkovs.shell.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.repository.AuthorRepository;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@ShellComponent
public class AuthorUpdateCommands {
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorUpdateCommands(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String aboutLink;

    private Map<String, Object> fieldsView = new LinkedHashMap<>();

    @ShellMethod("set id [id]")
    public void setAuthorId(long id) {
        this.id = id;
    }

    @ShellMethod("set first name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @ShellMethod("set last name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ShellMethod("set birth date in format [YYYY-MM-DD]")
    public void setBirthDate(String birthDate) {
        try {
            this.birthDate = LocalDate.parse(birthDate);
        } catch (DateTimeParseException dtpe) {
            throw new IllegalArgumentException("Couldn't parse birth date [%s]. " +
                    "Please specify birth date in format [YYYY-MM-DD]");
        }
    }

    @ShellMethod("set about link")
    public void setAuthorAboutLink(String aboutLink) {
        this.aboutLink = aboutLink;
    }

    @ShellMethod("get current data")
    public Map<String, Object> getAuthorCurrentData() {
        fieldsView.put("id", id);
        fieldsView.put("first name", firstName);
        fieldsView.put("last name", lastName);
        fieldsView.put("birth date", birthDate);
        fieldsView.put("about link", aboutLink);
        return fieldsView;
    }

    protected void clearCurrentData() {
        id = null;
        firstName = null;
        lastName = null;
        birthDate = null;
        aboutLink = null;
    }

    @ShellMethod("save author")
    @ShellMethodAvailability("checkNames")
    public String saveAuthor() {
        Author author = createAuthor();
        Optional<Author> foundAuthor = authorRepository.findById(author.getId());
        String operation;
        if (foundAuthor.isPresent()) {
            operation = "Updated";
        } else {
            operation = "Added";
        }
        Author savedAuthor = authorRepository.save(author);
        clearCurrentData();
        return operation + " author: " + savedAuthor;
    }

    private Author createAuthor() {
        Author author = new Author(firstName, lastName);
        if (id != null) {
            author.setId(id);
        }
        author.setBirthDate(birthDate);
        author.setAboutLink(aboutLink);
        return author;
    }

    public Availability checkIdAndNames() {
        if (id == null) {
            return Availability.unavailable("id not specified");
        }
        return checkNames();
    }

    public Availability checkNames() {
        if (firstName == null) {
            return Availability.unavailable("first name not specified");
        }
        if (lastName == null) {
            return Availability.unavailable("last name not specified");
        }
        return Availability.available();
    }
}
