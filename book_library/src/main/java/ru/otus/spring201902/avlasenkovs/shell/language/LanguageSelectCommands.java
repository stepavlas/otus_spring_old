package ru.otus.spring201902.avlasenkovs.shell.language;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Language;
import ru.otus.spring201902.avlasenkovs.repository.LangRepository;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class LanguageSelectCommands {
    private LangRepository langRepository;

    @Autowired
    public LanguageSelectCommands(LangRepository langRepository) {
        this.langRepository = langRepository;
    }

    @ShellMethod("view all available languages")
    public List<String> findAllLanguages() {
        List<Language> languages = langRepository.findAll();
        return languages.stream().map(Language::getName).collect(Collectors.toList());
    }
}
