package ru.otus.spring201902.avlasenkovs.model;

import ru.otus.spring201902.avlasenkovs.repository.converter.YearConverter;

import javax.persistence.*;
import java.time.Year;
import java.util.Objects;

@Entity
@NamedEntityGraph(name = "book-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("language"),
                @NamedAttributeNode("genre"),
                @NamedAttributeNode("author")
        })
@Table(name="book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;
    @Column(name="name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "lang_fk")
    private Language language;
    @Column(name="year")
    @Convert(converter = YearConverter.class)
    private Year year;
    @ManyToOne
    @JoinColumn(name = "genre_fk")
    private Genre genre;
    @ManyToOne
    @JoinColumn(name="author_fk")
    private Author author;
    @Column(name="about_link")
    private String aboutLink;

    public Book() {
    }

    public Book(long id, String name, Language language, Year year, Genre genre, Author author,
                String aboutLink) {
        this.id = id;
        this.name = name;
        this.language = language;
        this.year = year;
        this.genre = genre;
        this.author = author;
        this.aboutLink = aboutLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                Objects.equals(name, book.name) &&
                Objects.equals(language, book.language) &&
                Objects.equals(year, book.year) &&
                Objects.equals(genre, book.genre) &&
                Objects.equals(author, book.author) &&
                Objects.equals(aboutLink, book.aboutLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, language, year, genre, author, aboutLink);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getAboutLink() {
        return aboutLink;
    }

    public void setAboutLink(String aboutLink) {
        this.aboutLink = aboutLink;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", language=" + language +
                ", year=" + year +
                ", genre=" + genre +
                ", author=" + author +
                ", aboutLink='" + aboutLink + '\'' +
                '}';
    }

    public static class BookBuilder {
        private long id;
        private String name;
        private Language language;
        private Year year;
        private Genre genre;
        private Author author;
        private String aboutLink;

        public static BookBuilder book() {
            return new BookBuilder();
        }

        public BookBuilder id(long id) {
            this.id = id;
            return this;
        }

        public BookBuilder name(String name) {
            this.name = name;
            return this;
        }

        public BookBuilder language(Language language) {
            this.language = language;
            return this;
        }

        public BookBuilder year(Year year) {
            this.year = year;
            return this;
        }

        public BookBuilder genre(Genre genre) {
            this.genre = genre;
            return this;
        }

        public BookBuilder author(Author author) {
            this.author = author;
            return this;
        }

        public BookBuilder aboutLink(String aboutLink) {
            this.aboutLink = aboutLink;
            return this;
        }

        public Book build() {
            return new Book(id, name, language, year, genre, author, aboutLink);
        }
    }
}
