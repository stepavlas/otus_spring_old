package ru.otus.spring201902.avlasenkovs.shell.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.repository.AuthorRepository;

import java.util.Optional;

@ShellComponent
public class AuthorDeleteCommands {
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorDeleteCommands(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @ShellMethod("delete author by id")
    public String deleteAuthor(long id) {
        Optional<Author> author = authorRepository.findById(id);
        if (author.isPresent()) {
            authorRepository.delete(author.get());
            return "Deleted author: " + author;
        }
        return String.format("Author not deleted. Author with id [%d] not found", id);
    }
}
