package ru.otus.spring201902.avlasenkovs.shell.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.repository.AuthorRepository;

import java.util.Optional;

@ShellComponent
public class AuthorSelectCommands {
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorSelectCommands(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @ShellMethod("find author by id")
    public String findAuthorById(long id) {
        Optional<Author> author = authorRepository.findById(id);
        return author.map(Author::toString).orElseGet(() -> String.format("No author with id [%d] found.", id));
    }

    @ShellMethod("find author by first name and last name")
    public String findAuthorByName(String firstName, String lastName) {
        Optional<Author> author = authorRepository.findByName(firstName, lastName);
        return author.map(Author::toString)
                .orElseGet(() -> String.format("No author with first name [%s] and last name [%s] found.",
                        firstName, lastName));
    }
}
