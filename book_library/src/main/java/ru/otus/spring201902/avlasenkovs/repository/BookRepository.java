package ru.otus.spring201902.avlasenkovs.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.otus.spring201902.avlasenkovs.model.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends CrudRepository<Book, Long> {

    @EntityGraph(value = "book-entity-graph")
    @Override
    Optional<Book> findById(Long id);

    @Query("SELECT b FROM Book b LEFT JOIN FETCH b.language " +
            "LEFT JOIN FETCH b.genre LEFT JOIN FETCH b.author WHERE UPPER(b.name) LIKE CONCAT('%', UPPER(:bookName), '%')")
    List<Book> findBooksByName(@Param("bookName") String bookName);
}
