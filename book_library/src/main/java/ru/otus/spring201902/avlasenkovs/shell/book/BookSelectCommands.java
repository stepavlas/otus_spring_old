package ru.otus.spring201902.avlasenkovs.shell.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@ShellComponent
public class BookSelectCommands {
    private final BookRepository bookRepository;

    @Autowired
    public BookSelectCommands(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @ShellMethod("find book by id")
    public String findBookById(long id) {
        Optional<Book> book = bookRepository.findById(id);
        return book.map(Book::toString).orElseGet(() -> String.format("Book with id [%d] not found", id));
    }

    @ShellMethod("search for books which has name as part of the caption")
    public String searchBooksByName(String name) {
        List<Book> books = bookRepository.findBooksByName(name);
        return !books.isEmpty()? books.toString() : "No books were found";
    }
}
