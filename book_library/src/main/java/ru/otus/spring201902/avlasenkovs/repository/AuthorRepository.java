package ru.otus.spring201902.avlasenkovs.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.otus.spring201902.avlasenkovs.model.Author;

import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    @Query("SELECT a FROM Author a WHERE a.firstName = :firstName AND a.lastName = :lastName")
    Optional<Author> findByName(@Param("firstName") String firstName, @Param("lastName") String lastName);
}
