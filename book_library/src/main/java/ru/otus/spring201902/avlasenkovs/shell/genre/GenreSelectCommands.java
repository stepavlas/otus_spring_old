package ru.otus.spring201902.avlasenkovs.shell.genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Genre;
import ru.otus.spring201902.avlasenkovs.repository.GenreRepository;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class GenreSelectCommands {
    private GenreRepository genreRepository;

    @Autowired
    public GenreSelectCommands(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @ShellMethod("view all available genres")
    public List<String> findAllGenres() {
        List<Genre> genres = genreRepository.findAll();
        return genres.stream().map(Genre::getName).collect(Collectors.toList());
    }
}
