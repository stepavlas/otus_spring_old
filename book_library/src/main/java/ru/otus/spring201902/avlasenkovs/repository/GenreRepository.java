package ru.otus.spring201902.avlasenkovs.repository;

import org.springframework.data.repository.CrudRepository;
import ru.otus.spring201902.avlasenkovs.model.Genre;

import java.util.List;
import java.util.Optional;

public interface GenreRepository extends CrudRepository<Genre, Integer> {
    Optional<Genre> findByName(String genreName);

    @Override
    List<Genre> findAll();
}
