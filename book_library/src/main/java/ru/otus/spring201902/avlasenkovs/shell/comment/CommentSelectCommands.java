package ru.otus.spring201902.avlasenkovs.shell.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.model.Comment;
import ru.otus.spring201902.avlasenkovs.repository.CommentRepository;

import java.util.List;

@ShellComponent
public class CommentSelectCommands {
    private CommentRepository commentRepository;

    @Autowired
    public CommentSelectCommands(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @ShellMethod("find comments for book with specified id")
    public List<Comment> findComments(long bookId) {
        Book book = new Book();
        book.setId(bookId);
        return commentRepository.findCommentsByBook(book);
    }
}
