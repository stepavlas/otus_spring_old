package ru.otus.spring201902.avlasenkovs.shell.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.repository.BookRepository;

import java.util.Optional;

@ShellComponent
public class BookDeleteCommands {
    private final BookRepository bookRepository;

    @Autowired
    public BookDeleteCommands(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @ShellMethod("delete book by id")
    public String deleteBook(long id) {
        Optional<Book> book = bookRepository.findById(id);
        if (book.isEmpty()) {
            return String.format("Book not deleted. Book with id [%d] not found", id);
        }
        Book foundBook = book.get();
        bookRepository.delete(foundBook);
        return "Deleted book: " + foundBook;
    }
}
