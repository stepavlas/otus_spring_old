package ru.otus.spring201902.avlasenkovs.repository;

import org.springframework.data.repository.CrudRepository;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.model.Comment;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findCommentsByBook(Book book);
}
