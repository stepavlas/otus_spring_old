package ru.otus.spring201902.avlasenkovs.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="lang")
public class Language {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;
    @Column(name = "language")
    private String name;

    public Language() {
    }

    public Language(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Language language = (Language) o;
        return id == language.id &&
                Objects.equals(name, language.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public Language(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
