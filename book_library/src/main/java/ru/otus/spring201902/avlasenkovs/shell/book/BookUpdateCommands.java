package ru.otus.spring201902.avlasenkovs.shell.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.model.Genre;
import ru.otus.spring201902.avlasenkovs.model.Language;
import ru.otus.spring201902.avlasenkovs.repository.AuthorRepository;
import ru.otus.spring201902.avlasenkovs.repository.BookRepository;
import ru.otus.spring201902.avlasenkovs.repository.GenreRepository;
import ru.otus.spring201902.avlasenkovs.repository.LangRepository;

import java.time.Year;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static ru.otus.spring201902.avlasenkovs.model.Book.BookBuilder.book;

@ShellComponent
public class BookUpdateCommands {
    private final BookRepository bookRepository;
    private final LangRepository langRepository;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;

    private Long id;
    private String name;
    private Language language;
    private Year year;
    private Genre genre;
    private Author author;
    private String aboutLink;
    private Map<String, Object> fieldsView = new LinkedHashMap<>();

    @Autowired
    public BookUpdateCommands(BookRepository bookRepository,
                              LangRepository langRepository,
                              GenreRepository genreRepository,
                              AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.langRepository = langRepository;
        this.genreRepository = genreRepository;
        this.authorRepository = authorRepository;
    }

    @ShellMethod("set id")
    public void setBookId(long id) {
        this.id = id;
    }

    @ShellMethod("set name")
    public void setName(String name) {
        this.name = name;
    }

    @ShellMethod("find and set language")
    public String findLanguage(String language) {
        Optional<Language> foundLang = langRepository.findByName(language);
        if (foundLang.isEmpty()) {
            return String.format("No language [%s] was found.", language);
        }
        this.language = foundLang.get();
        return "Found language: " + language;
    }

    @ShellMethod("find and set genre")
    public String findGenre(String genre) {
        Optional<Genre> foundGenre = genreRepository.findByName(genre);
        if (foundGenre.isEmpty()) {
            return String.format("No genre [%s] was found.", genre);
        }
        this.genre = foundGenre.get();
        return "Found genre: " + genre;
    }

    @ShellMethod("find and set author by first name and last name")
    public String findAuthor(String firstName, String lastName) {
        Optional<Author> foundAuthor = authorRepository.findByName(firstName, lastName);
        if (foundAuthor.isEmpty()) {
            return String.format("No author with first name [%s] and last name [%s] was found.", firstName, lastName);
        }
        this.author = foundAuthor.get();
        return "Found author: " + author;
    }

    @ShellMethod("set year")
    public void setYear(int year) {
        this.year = Year.of(year);
    }

    @ShellMethod("set about link")
    public void setBookAboutLink(String aboutLink) {
        this.aboutLink = aboutLink;
    }


    @ShellMethod("save book")
    @ShellMethodAvailability({"checkName"})
    public String saveBook() {
        Book book = createBook();
        Optional<Book> foundBook = bookRepository.findById(book.getId());
        String operation;
        if (foundBook.isPresent()) {
            operation = "Updated";
        } else {
            operation = "Added";
        }
        Book newBook = bookRepository.save(book);
        clearCurrentData();
        return operation + " book: " + newBook;
    }

    private Book createBook() {
        Book book = book()
                .name(name)
                .language(language)
                .year(year)
                .genre(genre)
                .author(author)
                .aboutLink(aboutLink)
                .build();
        if (id != null) {
            book.setId(id);
        }
        return book;
    }

    public Availability checkName() {
        return name != null ? Availability.available() : Availability.unavailable("book name not specified");
    }

    @ShellMethod("get current data")
    protected Map<String, Object> getBookCurrentData() {
        fieldsView.put("id", id);
        fieldsView.put("name", name);
        fieldsView.put("language", language != null ? language.getName() : null);
        fieldsView.put("year", year);
        fieldsView.put("genre", genre != null ? genre.getName() : null);
        fieldsView.put("author", author != null ? author.getFirstName() + " " + author.getLastName() : null);
        fieldsView.put("about link", aboutLink);
        return fieldsView;
    }

    private void clearCurrentData() {
        id = null;
        name=null;
        language = null;
        year = null;
        genre = null;
        author = null;
        aboutLink = null;
    }
}
