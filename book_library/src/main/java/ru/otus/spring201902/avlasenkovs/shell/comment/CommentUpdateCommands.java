package ru.otus.spring201902.avlasenkovs.shell.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.model.Comment;
import ru.otus.spring201902.avlasenkovs.repository.BookRepository;
import ru.otus.spring201902.avlasenkovs.repository.CommentRepository;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@ShellComponent
public class CommentUpdateCommands {
    private final CommentRepository commentRepository;
    private final BookRepository bookRepository;

    private String content;
    private String user;
    private Book book;
    private Map<String, Object> fieldsView = new LinkedHashMap<>();

    @Autowired
    public CommentUpdateCommands(CommentRepository commentRepository,
                                 BookRepository bookRepository) {
        this.commentRepository = commentRepository;
        this.bookRepository = bookRepository;
    }

    public Availability checkFields() {
        if (content == null) {
            return Availability.unavailable("Content not specified.");
        } else if (user == null) {
            return Availability.unavailable("User not specified.");
        } else if (book == null) {
            return Availability.unavailable("Book not specified.");
        }
        return Availability.available();
    }

    @ShellMethod("set content")
    public void setContent(String content) {
        this.content = content;
    }

    @ShellMethod("set user")
    public void setUser(String user) {
        this.user = user;
    }

    @ShellMethod("find and set book for comment")
    public String findBookForComment(long id) {
        Optional<Book> foundBook = bookRepository.findById(id);
        if (foundBook.isPresent()) {
            this.book = foundBook.get();
            return "Found book: " + book.getName();
        } else {
            return String.format("No book with id [%d] was found.", id);
        }
    }

    @ShellMethod("get current data")
    public Map<String, Object> getCommentCurrentData() {
        fieldsView.put("content", content);
        fieldsView.put("user", user);
        fieldsView.put("book", book!= null ? book.getName() : null);
        return fieldsView;
    }

    @ShellMethod("add comment")
    @ShellMethodAvailability({"checkFields"})
    public String addComment() {
        Comment comment = new Comment(content, user);
        comment.setBook(book);
        Comment newComment = commentRepository.save(comment);
        clearCurrentData();
        return newComment != null ? "Comment added: " + newComment.toString() : "No comment added.";
    }

    private void clearCurrentData() {
        this.content = null;
        this.user = null;
        this.book = null;
    }
}
