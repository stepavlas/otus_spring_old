INSERT INTO genre
(genre)
VALUES
('Psychological Fiction'),
('Fantasy Fiction'),
('Satire'),
('Historical novel'),
('Realist novel');

INSERT INTO lang
(language)
VALUES
('chinese'),
('spanish'),
('english'),
('russian'),
('french');

INSERT INTO author
(first_name, last_name, birth_date, about_link)
VALUES
('Fyodor', 'Dostoevsky', '1821-11-11', 'https://en.wikipedia.org/wiki/Fyodor_Dostoevsky'),
('Mikhail', 'Bulgakov', '1891-05-15', 'https://en.wikipedia.org/wiki/Mikhail_Bulgakov'),
('Alexandre', 'Dumas', '1802-07-24', 'https://en.wikipedia.org/wiki/Alexandre_Dumas'),
('Victor', 'Hugo', '1802-02-26', 'https://en.wikipedia.org/wiki/Victor_Hugo'),
('Jack', 'London', '1876-01-12', 'https://en.wikipedia.org/wiki/Jack_London');

INSERT INTO book
(name, lang_fk, year, genre_fk, author_fk, about_link)
VALUES
('Crime and Punishment', 4, 1866, 1, 1, 'https://en.wikipedia.org/wiki/Crime_and_Punishment'),
('The Master and Margarita', 4, 1967, 2, 2, 'https://en.wikipedia.org/wiki/The_Master_and_Margarita'),
('The Three Musketeers', 5, 1844, 4, 3, 'https://en.wikipedia.org/wiki/The_Three_Musketeers'),
('The Count of Monte Cristo', 5, 1844, 4, 3, 'https://en.wikipedia.org/wiki/The_Count_of_Monte_Cristo'),
('Les Misérables', 5, 1862, 4, 4, 'https://en.wikipedia.org/wiki/Les_Mis%C3%A9rables'),
('The Hunchback of Notre Dame', 5, 1831, 1, 4, 'https://en.wikipedia.org/wiki/The_Hunchback_of_Notre-Dame'),
('White Fang', 3, 1906, 2, 5, 'https://en.wikipedia.org/wiki/White_Fang');

INSERT INTO comment
(comment, book_fk, user_name, datetime)
VALUES
('Read this book during boyhood. Want to reread it now.', 3, 'Pushkin fan', parsedatetime('07-06-2018 08:02:10 PM', 'dd-MM-yyyy hh:mm:ss a')),
('What a wonderful books, we need more books like this', 3, 'Bookinist Alexei', parsedatetime('11-02-2019 07:21:25 PM', 'dd-MM-yyyy hh:mm:ss a')),
('Absolute masterpiece. Lets all read this book', 1, 'Columb', parsedatetime('04-03-2019 09:06:34 PM', 'dd-MM-yyyy hh:mm:ss a')),
('Classics. Masterpiece!', 1, 'Reader', parsedatetime('24-02-2019 12:02:24 PM', 'dd-MM-yyyy hh:mm:ss a')),
('Strongly advise to read this book. French book are one of the best in the world.', 4, 'Hunter', parsedatetime('10-12-2018 09:06:34 PM', 'dd-MM-yyyy hh:mm:ss a')),
('I want to try to read it again later. Very difficult to read.', 5, 'Peter the Great', parsedatetime('02-09-2019 02:03:21 PM', 'dd-MM-yyyy hh:mm:ss a')),
('Fantastic novel and one of the best I books I have ever read.', 6, 'Batman', parsedatetime('15-03-2019 10:25:21 AM', 'dd-MM-yyyy hh:mm:ss a')),
('Some other fake comment. Maybe later delete it', 7, 'Test user', parsedatetime('07-02-2019 12:04:12 PM', 'dd-MM-yyyy hh:mm:ss a'));