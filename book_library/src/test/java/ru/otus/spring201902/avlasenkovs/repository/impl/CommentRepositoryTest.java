package ru.otus.spring201902.avlasenkovs.repository.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.otus.spring201902.avlasenkovs.model.*;
import ru.otus.spring201902.avlasenkovs.repository.CommentRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static ru.otus.spring201902.avlasenkovs.model.Book.BookBuilder.book;

@DisplayName("Repository for working with comments ")
@DataJpaTest
public class CommentRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("should find comments by book's id")
    @Test
    public void shouldFindCommentsByBooksId() {
        Book bookForSearch = new Book();
        bookForSearch.setId(1);
        List<Comment> comments = commentRepository.findCommentsByBook(bookForSearch);
        String user1 = "Columb";
        String user2 = "Reader";
        Comment actual1 = getCommentByUserFromList(comments, user1);
        Comment actual2 = getCommentByUserFromList(comments, user2);

        Book expectedBook = prepareBook();
        Comment expected1 = new Comment("Absolute masterpiece. Lets all read this book", user1);
        expected1.setId(3);
        expected1.setBook(expectedBook);
        expected1.setDateTime(LocalDateTime.of(2019, 3, 4, 21, 6, 34));
        Comment expected2 = new Comment("Classics. Masterpiece!", user2);
        expected2.setId(4);
        expected2.setBook(expectedBook);
        expected2.setDateTime(LocalDateTime.of(2019, 2, 24, 12, 2, 24));

        assertAll(
                () -> assertEquals(2, comments.size()),
                () -> Assertions.assertThat(actual1).isEqualToComparingFieldByFieldRecursively(expected1),
                () -> Assertions.assertThat(actual2).isEqualToComparingFieldByFieldRecursively(expected2)
        );
    }

    private Comment getCommentByUserFromList(List<Comment> comments, String user) {
        return comments.stream()
                .filter(comment -> user.equals(comment.getUser()))
                .findAny()
                .orElse(null);
    }

    private Book prepareBook() {
        Language language = new Language(4, "russian");
        Genre genre = new Genre(1, "Psychological Fiction");
        Author author = prepareAuthor();
        return book()
                .id(1)
                .name("Crime and Punishment")
                .language(language)
                .year(Year.of(1866))
                .genre(genre)
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Crime_and_Punishment")
                .build();
    }

    private Author prepareAuthor() {
        Author author = new Author("Fyodor", "Dostoevsky");
        author.setId(1);
        author.setBirthDate(LocalDate.of(1821, 11, 11));
        author.setAboutLink("https://en.wikipedia.org/wiki/Fyodor_Dostoevsky");
        return author;
    }

    @DisplayName("should return empty list of comments for not existing book")
    @Test
    public void shouldReturnEmptyListOfCommentsForNotExistingBook() {
        Book notExistingBook = new Book();
        notExistingBook.setId(100);
        List<Comment> comments = commentRepository.findCommentsByBook(notExistingBook);
        assertTrue(comments.isEmpty());
    }

    @DisplayName("should return empty list of comments for book without comments")
    @Test
    public void shouldReturnEmptyListOfCommentsForBookWithoutComments() {
        Book bookWithoutComments = new Book();
        bookWithoutComments.setId(2);
        List<Comment> comments = commentRepository.findCommentsByBook(bookWithoutComments);
        assertTrue(comments.isEmpty());
    }

    @DisplayName("should return empty list of comments when getting comments for book with negative id")
    @Test
    public void shouldReturnEmptyListOfCommentsWhenGettingCommentsForBookWithNegativeId() {
        Book bookWithNegativeId = new Book();
        bookWithNegativeId.setId(-2);
        List<Comment> commentsByBook = commentRepository.findCommentsByBook(bookWithNegativeId);
        assertTrue(commentsByBook.isEmpty());
    }

    @DisplayName("should return empty list when getting comments if null book passed")
    @Test
    public void shouldReturnEmptyListWhenGettingCommentsIfNullBookPassed() {
        List<Comment> commentsByBook = commentRepository.findCommentsByBook(null);
        assertTrue(commentsByBook.isEmpty());
    }

    @DisplayName("should add comment")
    @Test
    public void shouldAddComment() {
      checkAddingCommentWithId(null, "Add comment test without id.");
    }

    @DisplayName("should add comment with negative id")
    @Test
    public void shouldAddCommentWithNegativeId() {
        checkAddingCommentWithId(-10L, "Add comment test with negative id,");
    }

    @DisplayName("should add comment with existing id")
    @Test
    public void shouldAddCommentWithExistingId() {
        checkAddingCommentWithId(2L, "Add comment test with positive id.");
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsage' exception when null passed for adding comment")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionWhenNullPassedForAddingComment() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> commentRepository.save(null));
    }

    private void checkAddingCommentWithId(Long id, String content) {
        Comment comment = createCommentForAdding(content);
        if (id != null) {
            comment.setId(id);
        }

        Comment newComment = commentRepository.save(comment);

        comment.setId(newComment.getId());
        Comment foundComment = em.find(Comment.class, newComment.getId());
        assertAll(
                () -> Assertions.assertThat(newComment).isEqualToComparingFieldByFieldRecursively(comment),
                () -> Assertions.assertThat(foundComment).isEqualToComparingFieldByFieldRecursively(comment)
        );
    }

    private Comment createCommentForAdding(String content) {
        Comment comment = new Comment(content, "Artificial User");
        Book book = createBookForAddingComment();
        comment.setBook(book);
        return comment;
    }

    private Book createBookForAddingComment() {
        Language language = new Language(5, "french");
        Genre genre = new Genre(4, "Historical novel");
        Author author = new Author("Alexandre", "Dumas");
        author.setId(3);
        author.setBirthDate(LocalDate.of(1802, 7, 24));
        author.setAboutLink("https://en.wikipedia.org/wiki/Alexandre_Dumas");
        Book book = book()
                .id(3)
                .name("The Three Musketeers")
                .language(language)
                .year(Year.of(1844))
                .genre(genre)
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/The_Three_Musketeers")
                .build();
        return book;
    }

    @DisplayName("should throw 'DataIntegrityViolation' exception when adding comment without book")
    @Test
    public void shouldThrowDataIntegrityViolationExceptionWhenAddingCommentWithoutBook() {
        Comment comment = new Comment("This is comment without book reference", "Artificial User");
        assertThrows(DataIntegrityViolationException.class,
                () -> commentRepository.save(comment));
    }
}
