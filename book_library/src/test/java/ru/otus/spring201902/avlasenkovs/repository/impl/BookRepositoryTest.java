package ru.otus.spring201902.avlasenkovs.repository.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.model.Book;
import ru.otus.spring201902.avlasenkovs.model.Genre;
import ru.otus.spring201902.avlasenkovs.model.Language;
import ru.otus.spring201902.avlasenkovs.repository.BookRepository;

import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static ru.otus.spring201902.avlasenkovs.model.Book.BookBuilder.book;

@DisplayName("Repository for working with books ")
@DataJpaTest
public class BookRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("should find book by id")
    @Test
    public void shouldFindBookById() {
        long bookId = 7;
        Optional<Book> optionalBook = bookRepository.findById(bookId);
        if (optionalBook.isEmpty()) {
            fail(String.format("Book with id [%d] not found", bookId));
        }
        Book actual = optionalBook.get();
        Language language = new Language(3, "english");
        Genre genre = new Genre(2, "Fantasy Fiction");
        Author author = prepareAuthor2();
        Book expected = book()
                .id(bookId)
                .name("White Fang")
                .language(language)
                .year(Year.of(1906))
                .genre(genre)
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/White_Fang")
                .build();
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should find empty book if book with id not exists")
    @Test
    public void shouldFindEmptyBookIfBookWithIdNotExists() {
        Optional<Book> foundBook = bookRepository.findById(100L);
        assertThat(foundBook).isEmpty();
    }

    @DisplayName("should find empty book if negative id passed")
    @Test
    public void shouldFindEmptyBookIfNegativeIdPassed() {
        Optional<Book> foundBook = bookRepository.findById(-3L);
        assertThat(foundBook).isEmpty();
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' if id is null when searching book by id")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfIdIsNullWhenSearchingBookById() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> bookRepository.findById(null));
    }

    @DisplayName("should find books with passed substring in name")
    @Test
    public void shouldFindBooksWithPassedSubstringInName() {
        Language language = new Language(2, "spanish");
        Genre genre = new Genre(2,"Fantasy Fiction");
        Author author = prepareAuthor2();
        Book book = book()
                .name("Citizen Count")
                .language(language)
                .year(Year.of(1846))
                .genre(genre)
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Citizen_Count")
                .build();
        Book newBook = em.persist(book);

        List<Book> foundBooks = bookRepository.findBooksByName("Count");
        Book foundBook1 = findBookByNameInList(foundBooks, "The Count of Monte Cristo");
        Book foundBook2 = findBookByNameInList(foundBooks, "Citizen Count");

        Book expectedBook = prepareExpectedBook();
        assertAll(
                () -> assertEquals(2, foundBooks.size()),
                () -> Assertions.assertThat(foundBook1).isEqualToComparingFieldByFieldRecursively(expectedBook),
                () -> Assertions.assertThat(foundBook2).isEqualToComparingFieldByFieldRecursively(newBook)
        );
    }

    private Book findBookByNameInList(List<Book> books, String name) {
        return books.stream()
                .filter(foundBook -> name.equals(foundBook.getName()))
                .findAny()
                .orElse(null);
    }

    private Book prepareExpectedBook() {
        Language language = new Language(5, "french");
        Genre genre = new Genre(4, "Historical novel");
        Author author = prepareAuthor1();
        return book()
                .id(4)
                .name("The Count of Monte Cristo")
                .language(language)
                .year(Year.of(1844))
                .genre(genre)
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/The_Count_of_Monte_Cristo")
                .build();
    }

    @DisplayName("should return empty list if no books with passed substring found")
    @Test
    public void shouldReturnEmptyListIfNoBooksWithPassedSubstringFound() {
        List<Book> books = bookRepository.findBooksByName("quadcopter");
        assertTrue(books.isEmpty());
    }

    @DisplayName("should throw 'NullPointerException' if null passed when searching books by name")
    @Test
    public void shouldThrowNullPointerExceptionIfNullPassedWhenSearchingBooksByName() {
        List<Book> books = bookRepository.findBooksByName(null);
        assertTrue(books.isEmpty());
    }

    @DisplayName("should save book")
    @Test
    public void shouldSaveBook() {
        Author author = prepareAuthor1();
        Book book = book()
                .name("The Black Tulip")
                .language(new Language(5, "french"))
                .year(Year.of(1850))
                .genre(new Genre(5, "Realist novel"))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/The_Black_Tulip")
                .build();
        checkBookSave(book);
    }

    @DisplayName("should save book without none mandatory value")
    @Test
    public void shouldSaveBookWithoutNoneMandatoryValue() {
        Author author = prepareAuthor1();
        Book book = book()
                .name("Book Without Genre")
                .language(new Language(1, "chinese"))
                .year(Year.of(1850))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Book_Without_Genre")
                .build();
        checkBookSave(book);
    }

    @DisplayName("should save book with negative id")
    @Test
    public void shouldAddBookWithNegativeId() {
        Author author = prepareAuthor1();
        Book book = book()
                .id(-10)
                .name("Book With Negative Id")
                .language(new Language(1, "chinese"))
                .year(Year.of(1870))
                .genre(new Genre(2, "Fantasy Fiction"))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Book_With_Negative_Id")
                .build();
        checkBookSave(book);
    }

    private void checkBookSave(Book book) {
        Book addedBook = bookRepository.save(book);
        book.setId(addedBook.getId());
        Book foundBook = em.find(Book.class, addedBook.getId());
        assertAll(
                () -> Assertions.assertThat(addedBook).isEqualToComparingFieldByFieldRecursively(book),
                () -> Assertions.assertThat(foundBook).isEqualToComparingFieldByFieldRecursively(book)
        );
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' if null passed for saving book")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfNullPassedForSavingBook() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> bookRepository.save(null));
    }

    @DisplayName("should update existing book")
    @Test
    public void shouldUpdateExistingBook() {
        Author author = prepareAuthor1();
        long bookId = 3;
        Book book = book()
                .id(bookId)
                .name("The Three Musketeers")
                .language(new Language(5, "french"))
                .year(Year.of(1845))
                .genre(new Genre(1, "Psychological Fiction"))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/TheThreeMusketeers")
                .build();
        Book updatedBook = bookRepository.save(book);
        Book foundBook = em.find(Book.class, bookId);
        assertAll(
                () -> Assertions.assertThat(updatedBook).isEqualToComparingFieldByFieldRecursively(book),
                () -> Assertions.assertThat(foundBook).isEqualToComparingFieldByFieldRecursively(book)
        );
    }

    @DisplayName("should delete book")
    @Test
    public void shouldDeleteBook() {
        long bookId = 2;
        Book book = em.find(Book.class, bookId);
        if (book == null) {
            throw new IllegalStateException(String.format("Couldn't find book with id [%d] to delete from database.",
                    bookId));
        }
        bookRepository.delete(book);
        Book foundBook = em.find(Book.class, bookId);
        assertNull(foundBook);
    }

    @DisplayName("should not throw exception when deleting nonexistent book")
    @Test
    public void shouldNotThrowExceptionWhenDeletingNonexistentBook() {
        long bookId = 100;
        Book foundBook = em.find(Book.class, bookId);
        if (foundBook != null) {
            throw new IllegalStateException(String.format("Found book to delete from database with id [%d].",
                    bookId));
        }
        Author author = prepareAuthor1();
        Book book = book()
                .id(bookId)
                .name("Nonexistent Book")
                .language(new Language(1, "chinese"))
                .year(Year.of(1850))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Nonexistent_Book")
                .build();
        bookRepository.delete(book);
    }

    @DisplayName("should not throw exception when deleting book with negative id")
    @Test
    public void shouldNotThrowExceptionWhenDeletingBookWithNegativeId() {
        Author author = prepareAuthor3();
        Book book = book()
                .id(-10)
                .name("Book With Negative Id")
                .language(new Language(1, "chinese"))
                .year(Year.of(1870))
                .genre(new Genre(2, "Fantasy Fiction"))
                .author(author)
                .aboutLink("https://en.wikipedia.org/wiki/Book_With_Negative_Id")
                .build();
        Book foundBook = em.find(Book.class, book.getId());
        if (foundBook != null) {
            throw new IllegalStateException(String.format("Found book to delete from database with id [%d].",
                    book.getId()));
        }
        bookRepository.delete(book);
    }

    private Author prepareAuthor1() {
        Author author = new Author("Alexandre", "Dumas");
        author.setId(3);
        author.setBirthDate(LocalDate.of(1802, 7, 24));
        author.setAboutLink("https://en.wikipedia.org/wiki/Alexandre_Dumas");
        return author;
    }

    private Author prepareAuthor2() {
        Author author = new Author("Jack", "London");
        author.setId(5);
        author.setBirthDate(LocalDate.of(1876, 1, 12));
        author.setAboutLink("https://en.wikipedia.org/wiki/Jack_London");
        return author;
    }

    private Author prepareAuthor3() {
        Author author = new Author("Mikhail", "Bulgakov");
        author.setId(2);
        author.setBirthDate(LocalDate.of(1891, 5, 15));
        author.setAboutLink("https://en.wikipedia.org/wiki/Mikhail_Bulgakov");
        return author;
    }
}
