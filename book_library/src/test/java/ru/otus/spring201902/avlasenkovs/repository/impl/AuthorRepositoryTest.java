package ru.otus.spring201902.avlasenkovs.repository.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.otus.spring201902.avlasenkovs.model.Author;
import ru.otus.spring201902.avlasenkovs.repository.AuthorRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Repository for working with authors ")
@DataJpaTest
public class AuthorRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private TestEntityManager em;

    @DisplayName("should find author by id")
    @Test
    public void shouldFindAuthorById() {
        long authorId = 5;
        Optional<Author> optionalAuthor = authorRepository.findById(authorId);
        if (!optionalAuthor.isPresent()) {
            fail(String.format("Author with id [%d] not found.", authorId));
        }
        Author actual = optionalAuthor.get();
        Author expected = new Author("Jack", "London");
        expected.setId(authorId);
        expected.setBirthDate(LocalDate.of(1876, 1, 12));
        expected.setAboutLink("https://en.wikipedia.org/wiki/Jack_London");
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should find empty author if author with passed id not exists")
    @Test
    public void shouldFindEmptyAuthorIfAuthorWithPassedIdNotExists() {
        Optional<Author> foundAuthor = authorRepository.findById(100L);
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author for negative id")
    @Test
    public void shouldFindEmptyAuthorForNegativeId() {
        Optional<Author> foundAuthor = authorRepository.findById(-4L);
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' if id is null when searching author by id")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfIdIsNullWhenSearchingAuthorById() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> authorRepository.findById(null));
    }

    @DisplayName("should find author by first and last names only if both values fit")
    @Test
    public void shouldFindAuthorByFirstAndLastNames() {
        String firstName = "Mikhail";
        String lastName = "Bulgakov";
        Optional<Author> optionalAuthor = authorRepository.findByName(firstName, lastName);
        if (!optionalAuthor.isPresent()) {
            fail(String.format("Author with first name [%s] and last name [%s] not found.", firstName, lastName));
        }
        Author actual = optionalAuthor.get();
        Author expected = new Author(firstName, lastName);
        expected.setId(2);
        expected.setBirthDate(LocalDate.of(1891, 5, 15));
        expected.setAboutLink("https://en.wikipedia.org/wiki/Mikhail_Bulgakov");
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should get empty author if author with first and last names not exists")
    @Test
    public void shouldGetEmptyAuthorIfAuthorWithFirstAndLastNamesNotExists() {
        Optional<Author> foundAuthor = authorRepository.findByName("Sergei", "Writer");
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author if only one of the first or last names fits")
    @Test
    public void shouldFindEmptyAuthorIfOnlyOneOfTheFirstOrLastNamesFits() {
        Optional<Author> foundAuthor = authorRepository.findByName("Fyodor", "Bulgakov");
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author while searching author if first name is empty")
    @Test
    public void shouldFindEmptyAuthorWhileSearchingAuthorIfFirstNameIsEmpty() {
        Optional<Author> foundAuthor = authorRepository.findByName("", "Bulgakov");
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author while searching author if last name is empty")
    @Test
    public void shouldFindEmptyAuthorWhileSearchingAuthorIfLastNameIsEmpty() {
        Optional<Author> foundAuthor = authorRepository.findByName("Alexandre", "");
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author while searching author if first name is null")
    @Test
    public void shouldFindEmptyAuthorWhileSearchingAuthorIfFirstNameIsNull() {
        Optional<Author> foundAuthor = authorRepository.findByName(null, "Bulgakov");
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should find empty author while searching author if last name is null")
    @Test
    public void shouldFindEmptyAuthorWhileSearchingAuthorIfLastNameIsNull() {
        Optional<Author> foundAuthor = authorRepository.findByName("Alexandre", null);
        assertThat(foundAuthor).isEmpty();
    }

    @DisplayName("should save author")
    @Test
    public void shouldSaveAuthor() {
        Author author = createNewAuthor();
        Author createdAuthor = authorRepository.save(author);
        author.setId(createdAuthor.getId());
        Author foundAuthor = em.find(Author.class, createdAuthor.getId());
        assertAll(
                () -> Assertions.assertThat(createdAuthor).isEqualToComparingFieldByFieldRecursively(author),
                () -> Assertions.assertThat(foundAuthor).isEqualToComparingFieldByFieldRecursively(author)
        );
    }

    @DisplayName("should save author with negative id")
    @Test
    public void shouldSaveAuthorWithNegativeId() {
        checkSavingAuthorWithId(-2);
    }

    @DisplayName("should save author with existing id")
    @Test
    public void shouldSaveAuthorWithExistingId() {
        checkSavingAuthorWithId(3);
    }

    private void checkSavingAuthorWithId(long id) {
        Author author = createNewAuthor();
        author.setId(id);
        Author createdAuthor = authorRepository.save(author);
        author.setId(createdAuthor.getId());
        Author foundAuthor = em.find(Author.class, createdAuthor.getId());
        assertAll(
                () -> Assertions.assertThat(createdAuthor).isEqualToComparingFieldByFieldRecursively(author),
                () -> Assertions.assertThat(foundAuthor).isEqualToComparingFieldByFieldRecursively(author)
        );
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' exception if null value passed for adding author")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfNullPassedForSavingAuthor() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> authorRepository.save(null));
    }

    @DisplayName("should update existing author")
    @Test
    public void shouldUpdateExistingAuthor() {
        long authorId = 1;
        Author author = new Author("Fyodor", "Dostoevsky");
        author.setId(authorId);
        author.setBirthDate(LocalDate.of(1821, 11, 12));
        author.setAboutLink("https://en.wikipedia.org/wiki/FyodorDostoevskiy");
        Author updatedAuthor = authorRepository.save(author);
        Author foundAuthor = em.find(Author.class, authorId);
        assertAll(
                () ->  Assertions.assertThat(updatedAuthor).isEqualToComparingFieldByFieldRecursively(author),
                () -> Assertions.assertThat(foundAuthor).isEqualToComparingFieldByFieldRecursively(author)
        );
    }

    @DisplayName("should delete author")
    @Test
    public void shouldDeleteAuthor() {
        Author author = createNewAuthor();
        Author createdAuthor = em.persist(author);
        authorRepository.delete(createdAuthor);
        Author foundAuthor = em.find(Author.class, createdAuthor.getId());
        assertNull(foundAuthor);
    }

    @DisplayName("should not throw exception when deleting not existing author")
    @Test
    public void shouldNotThrowExceptionWhenDeletingNotExistingAuthor() {
        Author author = createNewAuthor();
        author.setId(52);
        authorRepository.delete(author);
    }

    @DisplayName("should not throw exception when deleting author with negative id")
    @Test
    public void shouldNotThrowExceptionWhenDeletingAuthorWithNegativeId() {
        Author author = createNewAuthor();
        author.setId(-2);
        authorRepository.delete(author);
    }

    private Author createNewAuthor() {
        Author author = new Author("New", "Author");
        author.setBirthDate(LocalDate.of(1564, 4, 23));
        author.setAboutLink("https://en.wikipedia.org/wiki/New_Author");
        return author;
    }


}
