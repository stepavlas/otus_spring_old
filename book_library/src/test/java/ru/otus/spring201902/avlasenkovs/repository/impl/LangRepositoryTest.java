package ru.otus.spring201902.avlasenkovs.repository.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.otus.spring201902.avlasenkovs.model.Language;
import ru.otus.spring201902.avlasenkovs.repository.LangRepository;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Repository for working with languages ")
@DataJpaTest
public class LangRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private LangRepository langRepository;

    @DisplayName("should find language by id")
    @Test
    public void shouldFindLanguageById() {
        int langId = 2;
        Optional<Language> optLang = langRepository.findById(langId);
        if (optLang.isEmpty()) {
            fail(String.format("Language with id [%d] not found.", langId));
        }
        Language actual = optLang.get();
        Language expected = new Language(langId, "spanish");
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should find empty language if language with id not exists")
    @Test
    public void shouldFindEmptyLanguageIfLanguageWithIdNotExists() {
        Optional<Language> language = langRepository.findById(100);
        assertThat(language).isEmpty();
    }

    @DisplayName("should find empty language if negative id passed")
    @Test
    public void shouldFindEmptyLanguageIfNegativeIdPassed() {
        Optional<Language> language = langRepository.findById(-10);
        assertThat(language).isEmpty();
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' if id is null when searching language by id")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfIdIsNullWhenSearchingLanguageById() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> langRepository.findById(null));
    }


    @DisplayName("should find language by name")
    @Test
    public void shouldFindLanguageByName() {
        String languageName = "french";
        Optional<Language> optLang = langRepository.findByName(languageName);
        if (optLang.isEmpty()) {
            fail(String.format("No language with name [%s] found", languageName));
        }
        Language actual = optLang.get();
        Language expected = new Language(5, languageName);
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should find empty language if language with name not exists")
    @Test
    public void shouldFindEmptyLanguageIfLanguageWithNameNotExists() {
        Optional<Language> language = langRepository.findByName("unknown language");
        assertThat(language).isEmpty();
    }

    @DisplayName("should find empty language when searching language by empty name")
    @Test
    public void shouldFindEmptyLanguageWhenSearchingLanguageByEmptyName() {
        Optional<Language> foundLanguage = langRepository.findByName("");
        assertThat(foundLanguage).isEmpty();
    }

    @DisplayName("should find empty language when searching language by null value")
    @Test
    public void shouldFindEmptyLanguageWhenSearchingLanguageByNullValue() {
        Optional<Language> foundLanguage = langRepository.findByName(null);
        assertThat(foundLanguage).isEmpty();
    }

    @DisplayName("should find all languages that exist")
    @Test
    public void shouldFindAllLanguagesThatExist() {
        List<Language> languages = langRepository.findAll();
        assertAll(
                () -> assertEquals(5, languages.size()),
                () -> assertThat(languages).extracting("id", "name")
                        .contains(tuple(1, "chinese"))
                        .contains(tuple(2, "spanish"))
                        .contains(tuple(3, "english"))
                        .contains(tuple(4, "russian"))
                        .contains(tuple(5, "french"))
        );
    }
}
