package ru.otus.spring201902.avlasenkovs.repository.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.otus.spring201902.avlasenkovs.model.Genre;
import ru.otus.spring201902.avlasenkovs.repository.GenreRepository;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Repository for working with genres ")
@DataJpaTest
public class GenreRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private GenreRepository genreRepository;

    @DisplayName("should find genre by id")
    @Test
    public void shouldFindGenreById() {
        int genreId = 5;
        Optional<Genre> optionalGenre = genreRepository.findById(genreId);
        if (optionalGenre.isEmpty()) {
            fail(String.format("Genre with id [%d] not found.", genreId));
        }
        Genre actual = optionalGenre.get();
        Genre expected = new Genre(genreId, "Realist novel");
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should throw 'InvalidDataAccessApiUsageException' if id is null when searching genre by id")
    @Test
    public void shouldThrowInvalidDataAccessApiUsageExceptionIfIdIsNullWhenSearchingGenreById() {
        assertThrows(InvalidDataAccessApiUsageException.class,
                () -> genreRepository.findById(null));
    }

    @DisplayName("should find empty genre if genre with id not exists")
    @Test
    public void shouldFindEmptyGenreIfGenreWithIdNotExists() {
        Optional<Genre> genre = genreRepository.findById(100);
        assertThat(genre).isEmpty();
    }

    @DisplayName("should find empty genre if negative id passed")
    @Test
    public void shouldFindEmptyGenreIfNegativeIdPassed() {
        Optional<Genre> genre = genreRepository.findById(-1);
        assertThat(genre).isEmpty();
    }

    @DisplayName("should find genre by name")
    @Test
    public void shouldFindGenreByName() {
        String genreName = "Historical novel";
        Optional<Genre> optionalGenre = genreRepository.findByName(genreName);
        if (optionalGenre.isEmpty()) {
            fail(String.format("Genre with name [%s] not found.", genreName));
        }
        Genre actual = optionalGenre.get();
        Genre expected = new Genre(4, genreName);
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    @DisplayName("should find empty genre if genre with name not exists")
    @Test
    public void shouldFindEmptyGenreIfGenreWithNameNotExists() {
        Optional<Genre> genre = genreRepository.findByName("Nonexistent genre");
        assertThat(genre).isEmpty();
    }

    @DisplayName("should find empty genre when getting genre by empty name")
    @Test
    public void shouldFindEmptyGenreWhenGettingGenreByEmptyName() {
        Optional<Genre> foundGenre = genreRepository.findByName("");
        assertThat(foundGenre).isEmpty();
    }

    @DisplayName("should find empty genre when getting genre by null value")
    @Test
    public void shouldFindEmptyGenreWhenGettingGenreByNullValue() {
        Optional<Genre> foundGenre = genreRepository.findByName(null);
        assertThat(foundGenre).isEmpty();
    }

    @DisplayName("should find all genres that exist")
    @Test
    public void shouldFindAllGenresThatExist() {
        List<Genre> genres = genreRepository.findAll();
        assertAll(
                () -> assertEquals(5, genres.size()),
                () -> assertThat(genres).extracting("id", "name")
                        .contains(tuple(1, "Psychological Fiction"))
                        .contains(tuple(2, "Fantasy Fiction"))
                        .contains(tuple(3, "Satire"))
                        .contains(tuple(4, "Historical novel"))
                        .contains(tuple(5, "Realist novel"))
        );
    }
}
